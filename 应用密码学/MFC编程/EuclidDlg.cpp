// EuclidDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "MFC编程.h"
#include "EuclidDlg.h"
#include "afxdialogex.h"


// EuclidDlg 对话框

IMPLEMENT_DYNAMIC(EuclidDlg, CDialogEx)

EuclidDlg::EuclidDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(EuclidDlg::IDD, pParent)
	, m_a(0)
	, m_b(0)
	, m_c(0)
{

}

EuclidDlg::~EuclidDlg()
{
}

void EuclidDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_a);
	DDX_Text(pDX, IDC_EDIT2, m_b);
	DDX_Text(pDX, IDC_EDIT3, m_c);
}


BEGIN_MESSAGE_MAP(EuclidDlg, CDialogEx)
	ON_BN_CLICKED(IDC_BUTTON1, &EuclidDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &EuclidDlg::OnBnClickedButton2)
END_MESSAGE_MAP()


// EuclidDlg 消息处理程序


void EuclidDlg::OnBnClickedButton1()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData(TRUE);
	long long a,b;
	a=m_a;
	b=m_b;
	if(a<b){
		m_c=a;
		a=b;
		b=m_c;
	}
	while(b)
	{
		m_c=a%b;
		a=b;
		b=m_c;
	}
	m_c=a;
	UpdateData(FALSE);
}


void EuclidDlg::OnBnClickedButton2()
{
	// TODO: 在此添加控件通知处理程序代码
	CMFC编程Dlg homePage;
	homePage.DoModal();
}
