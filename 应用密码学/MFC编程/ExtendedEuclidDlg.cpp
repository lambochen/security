// ExtendedEuclidDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "MFC编程.h"
#include "ExtendedEuclidDlg.h"
#include "afxdialogex.h"


// ExtendedEuclidDlg 对话框

IMPLEMENT_DYNAMIC(ExtendedEuclidDlg, CDialogEx)

ExtendedEuclidDlg::ExtendedEuclidDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(ExtendedEuclidDlg::IDD, pParent)
	, m_a(0)
	, m_b(0)
	, m_c(0)
	, m_d(0)
{

}

ExtendedEuclidDlg::~ExtendedEuclidDlg()
{
}

void ExtendedEuclidDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_a);
	DDX_Text(pDX, IDC_EDIT2, m_b);
	DDX_Text(pDX, IDC_EDIT3, m_c);
	DDX_Text(pDX, IDC_EDIT4, m_d);
}


BEGIN_MESSAGE_MAP(ExtendedEuclidDlg, CDialogEx)
	ON_BN_CLICKED(IDC_BUTTON1, &ExtendedEuclidDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &ExtendedEuclidDlg::OnBnClickedButton2)
END_MESSAGE_MAP()


// ExtendedEuclidDlg 消息处理程序


void ExtendedEuclidDlg::OnBnClickedButton1()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData(TRUE);
	long long x1,x2,x3,y1,y2,y3,q;
	x1=1; x2=0; y1=0; y2=1;
	x3=m_b; y3=m_a;
	while(1)
	{
		if(y3==0){
			m_c=x3;
			m_d=0;
			break;
		}
		if(y3==1){
			m_c=1;
			m_d=y2;
			break;
		}
		q=x3/y3;
		long long t1,t2,t3;
		t1=x1-q*y1;
		t2=x2-q*y2;
		t3=x3-q*y3;
		x1=y1; x2=y2; x3=y3;
		y1=t1; y2=t2; y3=t3;
	}
	UpdateData(FALSE);
}


void ExtendedEuclidDlg::OnBnClickedButton2()
{
	// TODO: 在此添加控件通知处理程序代码
	CMFC编程Dlg homePage;
	homePage.DoModal();
}
