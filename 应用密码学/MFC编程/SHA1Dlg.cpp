// SHA1Dlg.cpp : 实现文件
//

#include "stdafx.h"
#include "MFC编程.h"
#include "SHA1Dlg.h"
#include "afxdialogex.h"
#include <iomanip>
#include <string>
#include <sstream>

// SHA1Dlg 对话框

IMPLEMENT_DYNAMIC(SHA1Dlg, CDialogEx)

SHA1Dlg::SHA1Dlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(SHA1Dlg::IDD, pParent)
	, message(_T(""))
	, cipher(_T(""))
{

}

SHA1Dlg::~SHA1Dlg()
{
}

void SHA1Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, message);
	DDX_Text(pDX, IDC_EDIT2, cipher);
}


BEGIN_MESSAGE_MAP(SHA1Dlg, CDialogEx)
	ON_BN_CLICKED(IDC_BUTTON2, &SHA1Dlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON1, &SHA1Dlg::OnBnClickedButton1)
END_MESSAGE_MAP()


// SHA1Dlg 消息处理程序


void SHA1Dlg::OnBnClickedButton2()
{
	// TODO: 在此添加控件通知处理程序代码
	CMFC编程Dlg homePage;
	homePage.DoModal();
}

unsigned circleShift(const unsigned& word,const int& bits){
    return (word<<bits) | ((word)>>(32-bits));
}
  
/*unsigned sha1Fun(const unsigned& B,const unsigned& C,const unsigned& D,const unsigned& t){
  
    switch (t/20){
        case 0:     return (B & C) | ((~B) & D);
        case 2:     return (B & C) | (B & D) | (C & D);
        case 1:
        case 3:     return B ^ C ^ D;
    }
  
    return t;
}
CString sha1(const CString& strRaw){
 
    CString str(strRaw);
 
    str+=(unsigned char)(0x80);
 
    // 每个字节8位,所以要乘8,左移3位
	int temp;
	for(temp=0;str[temp]!='\0';temp++);
    while (temp<<3 % 512 != 448){
        str+=(char)0;
    }
 
    // 写入原始数据长度
	for(temp=0;strRaw[temp]!='\0';temp++);
    for (int i(56);i>=0;i-=8){
        str+=(unsigned char)((temp<<3) >> i);
    }
 
    const unsigned K[4]={0x5a827999,0x6ed9eba1,0x8f1bbcdc,0xca62c1d6};
    unsigned A(0x67452301),B(0xefcdab89),C(0x98badcfe),D(0x10325476),E(0xc3d2e1f0),T(0);
    unsigned W[80]={0};
 
    // 每次处理64字节,共512位
	for(temp=0;str[temp]!='\0';temp++);
    for (unsigned i(0);i!=temp;i+=64){
        // 前16个字为原始数据
        for (unsigned t(0);t!=16;++t){
            // 将4个8位数据放入一个32位变量中
            W[t]=((unsigned)str[i+4*t] & 0xff)<<24 |
                 ((unsigned)str[i+4*t+1] & 0xff)<<16 |
                 ((unsigned)str[i+4*t+2] & 0xff)<<8 |
                 ((unsigned)str[i+4*t+3] & 0xff);
        }
 
        // 填充
        for (unsigned t(16);t!=80;++t){
            W[t]=circleShift(W[t-3] ^ W[t-8] ^ W[t-14] ^ W[t-16],1);
        }
 
        for (unsigned t(0);t!=80;++t){
            T=circleShift(A,5) + sha1Fun(B,C,D,t) + E + W[t] + K[t/20];
            E=D; 
            D=C; 
            C=circleShift(B,30); 
            B=A; 
            A=T;
        }
 
        A+=0x67452301;
        B+=0xefcdab89;
        C+=0x98badcfe;
        D+=0x10325476;
        E+=0xc3d2e1f0;
    }
 
    /*stringstream ss;
    ss<<setw(8)<<setfill('0')<<hex<<A<<B<<C<<D<<E;
    ss>>str;*/
	/*CString st;
	st.Format(_T("%x"),A);
	str+=st;
	st.Format(_T("%x"),B);
	str+=st;
	st.Format(_T("%x"),C);
	str+=st;
	st.Format(_T("%x"),D);
	str+=st;
	st.Format(_T("%x"),E);
	str+=st;

    
    return str;
}
*/



#include <stdio.h>
#include <string.h>
#define S(x,n)	(((x)<<n)|(x)>>(32-n))	//SHA定义S函数为循环左移 
static unsigned long h[5];
static unsigned long m[16];
static unsigned long w[80]; //补位(余数=448)+补长度(64位)=512位M 
static void sha1_pad(char *input, int len);
static void sha1_prepare(void);
static void sha1_calc(void);
unsigned long* sha1(char * input, int len);

static void sha1_pad(char *input, int len)
{
	int i; 	int n;
	for (i = 0; i < 16; i++)
	{
		m[i] = 0;
	}
	for (i = 0; i < len; i++) {
		n = 24 - ((i & 0x03) << 3);
		m[i / 4] |= input[i] << n;
	}
	n = 24 - ((i & 0x03) << 3);
	m[i / 4] |= 0x80 << n;
	m[15] = len * 8;
}//由512位M生成80字W 
static void sha1_prepare(void) {
	int i;
	for (i = 0; i < 16; i++)
	{
		w[i] = m[i];
	}
	for (i = 16; i < 80; i++) {
		w[i] = w[i - 16] ^ w[i - 14] ^ w[i - 8] ^ w[i - 3];
		w[i] = S(w[i], 1);
	}
} //由80字W计算sha1 
static void sha1_calc(void) {
	int i;
	unsigned long a, b, c, d, e, f, k;
	unsigned long temp;
	h[0] = 0x67452301;
	h[1] = 0xEFCDAB89;
	h[2] = 0x98BADCFE;
	h[3] = 0x10325476;
	h[4] = 0xC3D2E1F0;
	a = h[0];
	b = h[1];
	c = h[2];
	d = h[3];
	e = h[4];
	for (i = 0; i < 80; i++)
	{
		switch (i / 20)
		{
		case 0:
			k = 0x5A827999;
			f = (b&c) | (~b&d);
			break;
		case 1:
			k = 0x6ED9EBA1;
			f = b^c^d;
			break;
		case 2:
			k = 0x8F1BBCDC;
			f = (b&c) | (b&d) | (c&d);
			break;
		case 3:
			k = 0xCA62C1D6;
			f = b^c^d;
			break;
		}
		temp = S(a, 5) + f + e + w[i] + k;
		e = d;
		d = c;
		c = S(b, 30);
		b = a;
		a = temp;
	}
	h[0] += a;
	h[1] += b;
	h[2] += c;
	h[3] += d;
	h[4] += e;
}//SHA1算法接口
 //input:待校验的数据 
 //len:数据长度(小于56字节)
unsigned long* sha1(char * input, int len) {
	sha1_pad(input, len);
	sha1_prepare();
	sha1_calc();
	return h;
}
CString Transfer(unsigned long num) {
	int yushu, a[100];
	int i = 0, temp1 = 0, m = 0, temp2;
	char hex[16] = { '0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F' };
	char b[17];
	CString str;
	while (num>0)
	{
		yushu = num % 16;
		a[i++] = yushu;
		num = num / 16;

	}
	temp2 = temp1 = i;
	for (i = 0; i <temp1; i++)//倒序输出

	{
		m = a[i];
		b[i] = hex[m];
	}
	for (i = 0; i <temp2 / 2; i++) {
		b[temp2] = b[i];
		b[i] = b[temp2 - i - 1];
		b[temp2 - i - 1] = b[temp2];
	}
	b[temp1] = '\0';
	str = b;
	return str;
}
void SHA1Dlg::OnBnClickedButton1()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData(true);
	char *str;
	unsigned long * mac;
	CString str1[5];
	UpdateData(TRUE);
	USES_CONVERSION;
	str = T2A(message);//明文Cstring转化成char
	mac = sha1(str, strlen(str));
	for (int i = 0; i<5; i++)
		str1[i] = Transfer(mac[i]);
	cipher = str1[0] + str1[1] + str1[2] + str1[3] + str1[4];
	UpdateData(FALSE);
	// TODO: 在此添加控件通知处理程序代码
	//CDialogEx::OnOK();

	UpdateData(false);
	// TODO: 在此添加控件通知处理程序代码


}
