#pragma once


// SHA1Dlg 对话框

class SHA1Dlg : public CDialogEx
{
	DECLARE_DYNAMIC(SHA1Dlg)

public:
	SHA1Dlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~SHA1Dlg();

// 对话框数据
	enum { IDD = IDD_SHA1_Dlg };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton2();
	CString message;
	CString cipher;
	afx_msg void OnBnClickedButton1();
};

#include "MFC编程Dlg.h"
