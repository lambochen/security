// RSADlg.cpp : 实现文件
//

#include "stdafx.h"
#include "MFC编程.h"
#include "RSADlg.h"
#include "afxdialogex.h"


// RSADlg 对话框

IMPLEMENT_DYNAMIC(RSADlg, CDialogEx)

RSADlg::RSADlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(RSADlg::IDD, pParent)
	, message(_T(""))
	, cipher(_T(""))
{

}

RSADlg::~RSADlg()
{
}

void RSADlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, message);
	DDX_Text(pDX, IDC_EDIT2, cipher);
}


BEGIN_MESSAGE_MAP(RSADlg, CDialogEx)
	ON_BN_CLICKED(IDC_BUTTON2, &RSADlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON1, &RSADlg::OnBnClickedButton1)
END_MESSAGE_MAP()


// RSADlg 消息处理程序


void RSADlg::OnBnClickedButton2()
{
	// TODO: 在此添加控件通知处理程序代码
	CMFC编程Dlg homePage;
	homePage.DoModal();
}

int gcd(int a,int b){
    int t;
    while(b){
        t=a;
        a=b;
        b=t%b;
    }
    return a;
}
bool prime_w(int a,int b){
    if(gcd(a,b) == 1)
    return true;
    else
    return false;
}
int mod_inverse(int a,int r){
    int b=1;
    while(((a*b)%r)!=1){
        b++;
        if(b<0){
            //printf("error ,function can't find b ,and now b is negative number");
            return -1;
        }
    }
    return b;
} 
bool prime(int i){ 
    if(i<=1)
        return false;    
    for(int j=2;j<i;j++){
        if(i%j==0)return false;
    }
    return true;
}
void secret_key(int* p, int *q){
    int s = time(0);
    srand(s);
    do{ 
        *p = rand()%50+1;
    }while(!prime(*p));
    do{
        *q = rand()%50+1;
    }while( p==q || !prime(*q ));
}
int getRand_e(int r){
    int e = 2;
    while( e<1||e>r|| !prime_w(e,r) ){
        e++;
        if(e<0){
            //printf("error ,function can't find e ,and now e is negative number");
            return -1;
        }
    }
    return e;
}
int rsa(int a,int b,int c){ 
    int aa = a,r=1;
    b=b+1;
    while(b!=1){
        r=r*aa;
        r=r%c;
        b--;
    }
    return r;
}
int getlen(char *str){
    int i=0;
    while(str[i]!='\0'){
        i++;
        if(i<0)return -1;
    }
    return i;
}

void RSADlg::OnBnClickedButton1()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData(1);
	int p , q , N , r , e , d;
    p=0,q=0,N=0,e=0,d=0;
    secret_key(&p,&q);
    N = p*q;
    r = (p-1)*(q-1);
    e = getRand_e(r);
    d = mod_inverse(e,r);
    char mingwen,jiemi;
    int miwen;
    char mingwenStr[1024],jiemiStr[1024];
    int mingwenStrlen;
    int *miwenBuff;
	for(int i=0;message[i]!='\0';i++)
		mingwenStr[i]=message[i];
    mingwenStrlen = getlen(mingwenStr);
    miwenBuff = (int*)malloc(sizeof(int)*mingwenStrlen);
	CString m_cipher,str;
    for(int i = 0;i<mingwenStrlen;i++){
        miwenBuff[i] = rsa((int)mingwenStr[i],e,N);
		str.Format(_T("%d"),miwenBuff[i]);
		m_cipher+=str;
		m_cipher+=" ";
    }
	cipher=m_cipher;

	UpdateData(0);
}
