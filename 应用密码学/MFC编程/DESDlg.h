#pragma once


// DESDlg 对话框

class DESDlg : public CDialogEx
{
	DECLARE_DYNAMIC(DESDlg)

public:
	DESDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~DESDlg();

// 对话框数据
	enum { IDD = IDD_DES_Dlg };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CString m_message;
	CString m_key;
	CString m_cipher;
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
};

#include "MFC编程Dlg.h"