// DESDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "MFC编程.h"
#include "DESDlg.h"
#include "afxdialogex.h"


// DESDlg 对话框

IMPLEMENT_DYNAMIC(DESDlg, CDialogEx)

DESDlg::DESDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(DESDlg::IDD, pParent)
	, m_message(_T(""))
	, m_key(_T(""))
	, m_cipher(_T(""))
{

}

DESDlg::~DESDlg()
{
}

void DESDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_message);
	DDX_Text(pDX, IDC_EDIT2, m_key);
	DDX_Text(pDX, IDC_EDIT3, m_cipher);
}


BEGIN_MESSAGE_MAP(DESDlg, CDialogEx)
	ON_BN_CLICKED(IDC_BUTTON1, &DESDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &DESDlg::OnBnClickedButton2)
END_MESSAGE_MAP()


// DESDlg 消息处理程序
#include"stdafx.h"
#include <stdlib.h>
#include <stdio.h>
#include<iostream>   // 位处理 
#ifndef _TABLES_H_    // 防重复编译 
#define _TABLES_H_  

// 对明文执行IP置换得到L0,R0 （L左32位,R右32位）               [明文操作]
const char IP_Table[64] = {
	58,50,42,34,26,18,10, 2,60,52,44,36,28,20,12, 4,
	62,54,46,38,30,22,14, 6,64,56,48,40,32,24,16, 8,
	57,49,41,33,25,17, 9, 1,59,51,43,35,27,19,11, 3,
	61,53,45,37,29,21,13, 5,63,55,47,39,31,23,15, 7
};

// 对迭代后的L16,R16执行IP逆置换,输出密文
const char IPR_Table[64] = {
	40, 8,48,16,56,24,64,32,39, 7,47,15,55,23,63,31,
	38, 6,46,14,54,22,62,30,37, 5,45,13,53,21,61,29,
	36, 4,44,12,52,20,60,28,35, 3,43,11,51,19,59,27,
	34, 2,42,10,50,18,58,26,33, 1,41, 9,49,17,57,25
};

/*--------------------------- 迭代法则 ----------------------------*/

// F函数,32位的R0进行E变换,扩为48位输出 (R1~R16)        [备用A]  [明文操作] 
static char E_Table[48] = {
	32, 1, 2, 3, 4, 5, 4, 5, 6, 7, 8, 9,
	8, 9,10,11,12,13,12,13,14,15,16,17,
	16,17,18,19,20,21,20,21,22,23,24,25,
	24,25,26,27,28,29,28,29,30,31,32, 1
};

// 子密钥K(i)的获取 密钥为K 抛弃第6,16,24,32,40,48,64位          [密钥操作] 
// 用PC1选位 分为 前28位C0,后28位D0 两部分  
static char PC1_Table[56] = {
	57,49,41,33,25,17, 9, 1,58,50,42,34,26,18,
	10, 2,59,51,43,35,27,19,11, 3,60,52,44,36,
	63,55,47,39,31,23,15, 7,62,54,46,38,30,22,
	14, 6,61,53,45,37,29,21,13, 5,28,20,12, 4
};

// 对C0,D0分别进行左移,共16次,左移位数与下面对应                 [密钥操作]
static char Move_Table[16] = {
	1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1
};

// C1,D1为第一次左移后得到,进行PC2选位,得到48位输出K1   [备用B]   [密钥操作]     
static char PC2_Table[48] = {
	14,17,11,24, 1, 5, 3,28,15, 6,21,10,
	23,19,12, 4,26, 8,16, 7,27,20,13, 2,
	41,52,31,37,47,55,30,40,51,34,33,48,
	44,49,39,56,34,53,46,42,50,36,29,32
};

/*------------- F函数 备用A和备用B 异或 得到48位输出 ---------------*/

// 异或后的结果48位分为8组,每组6位,作为8个S盒的输入             [组合操作] 
// S盒以6位作为输入(8组),4位作为输出(4*(8组)=32位)
// S工作原理 假设输入为A=abcdef ,则bcde所代表的数是0-15之间的
// 一个数记为 X=bcde ,af代表的是0-3之间的一个数,记为 Y=af 
// 在S1的X列,Y行找到一个数Value,它在0-15之间,可以用二进制表示
// 所以为4bit (共32位)  
static char S_Box[8][4][16] = {
	//S1
	14, 4,13, 1, 2,15,11, 8, 3,10, 6,12, 5, 9, 0, 7,
	0,15, 7, 4,14, 2,13, 1,10, 6,12,11, 9, 5, 3, 8,
	4, 1,14, 8,13, 6, 2,11,15,12, 9, 7, 3,10, 5, 0,
	15,12, 8, 2, 4, 9, 1, 7, 5,11, 3,14,10, 0, 6,13,
	//S2
	15, 1, 8,14, 6,11, 3, 4, 9, 7, 2,13,12, 0, 5,10,
	3,13, 4, 7,15, 2, 8,14,12, 0, 1,10, 6, 9,11, 5,
	0,14, 7,11,10, 4,13, 1, 5, 8,12, 6, 9, 3, 2,15,
	13, 8,10, 1, 3,15, 4, 2,11, 6, 7,12, 0, 5,14, 9,
	//S3
	10, 0, 9,14, 6, 3,15, 5, 1,13,12, 7,11, 4, 2, 8,
	13, 7, 0, 9, 3, 4, 6,10, 2, 8, 5,14,12,11,15, 1,
	13, 6, 4, 9, 8,15, 3, 0,11, 1, 2,12, 5,10,14, 7,
	1,10,13, 0, 6, 9, 8, 7, 4,15,14, 3,11, 5, 2,12,
	//S4
	7,13,14, 3, 0, 6, 9,10, 1, 2, 8, 5,11,12, 4,15,
	13, 8,11, 5, 6,15, 0, 3, 4, 7, 2,12, 1,10,14, 9,
	10, 6, 9, 0,12,11, 7,13,15, 1, 3,14, 5, 2, 8, 4,
	3,15, 0, 6,10, 1,13, 8, 9, 4, 5,11,12, 7, 2,14,
	//S5
	2,12, 4, 1, 7,10,11, 6, 8, 5, 3,15,13, 0,14, 9,
	14,11, 2,12, 4, 7,13, 1, 5, 0,15,10, 3, 9, 8, 6,
	4, 2, 1,11,10,13, 7, 8,15, 9,12, 5, 6, 3, 0,14,
	11, 8,12, 7, 1,14, 2,13, 6,15, 0, 9,10, 4, 5, 3,
	//S6
	12, 1,10,15, 9, 2, 6, 8, 0,13, 3, 4,14, 7, 5,11,
	10,15, 4, 2, 7,12, 0, 5, 6, 1,13,14, 0,11, 3, 8,
	9,14,15, 5, 2, 8,12, 3, 7, 0, 4,10, 1,13,11, 6,
	4, 3, 2,12, 9, 5,15,10,11,14, 1, 7, 6, 0, 8,13,
	//S7
	4,11, 2,14,15, 0, 8,13, 3,12, 9, 7, 5,10, 6, 1,
	13, 0,11, 7, 4, 0, 1,10,14, 3, 5,12, 2,15, 8, 6,
	1, 4,11,13,12, 3, 7,14,10,15, 6, 8, 0, 5, 9, 2,
	6,11,13, 8, 1, 4,10, 7, 9, 5, 0,15,14, 2, 3,12,
	//S8
	13, 2, 8, 4, 6,15,11, 1,10, 9, 3,14, 5, 0,12, 7,
	1,15,13, 8,10, 3, 7, 4,12, 5, 6,11, 0,14, 9, 2,
	7,11, 4, 1, 9,12,14, 2, 0, 6,10,13,15, 3, 5, 8,
	2, 1,14, 7, 4,10, 8,13,15,12, 9, 0, 3, 5, 6,11
};

// F函数 最后第二步,对S盒输出的32进行P置换                     [组合操作]
// 输出的值参与一次迭代:
// L(i)=R(i-1)
// R(i)=L(i-1)^f(R(i-1),K(i)) 异或 
static char P_Table[32] = {
	16, 7,20,21,29,12,28,17, 1,15,23,26, 5,18,31,10,
	2, 8,24,14,32,27, 3, 9,19,13,30, 6,22,11, 4,25
};

// 16个子密钥K(1~16) 
static bool SubKey[16][48] = { 0 };

#endif
using namespace std;
void BitsCopy(bool *DatOut, bool *DatIn, int Len);  // 数组复制 

void ByteToBit(bool *DatOut, char *DatIn, int Num); // 字节到位 
void BitToByte(char *DatOut, bool *DatIn, int Num); // 位到字节

void BitToHex(char *DatOut, bool *DatIn, int Num);  // 二进制到十六进制 64位 to 4*16字符
void HexToBit(bool *DatOut, char *DatIn, int Num);  // 十六进制到二进制 

void TablePermute(bool *DatOut, bool *DatIn, const char *Table, int Num); // 位表置换函数 
void LoopMove(bool *DatIn, int Len, int Num);     // 循环左移 Len长度 Num移动位数 
void Xor(bool *DatA, bool *DatB, int Num);         // 异或函数 

void S_Change(bool DatOut[32], bool DatIn[48]);   // S盒变换 
void F_Change(bool DatIn[32], bool DatKi[48]);    // F函数                                  

void SetKey(char KeyIn[8]);                         // 设置密钥
void PlayDes(char MesOut[8], char MesIn[8]);       // 执行DES加密
void KickDes(char MesOut[8], char MesIn[8]);             // 执行DES解密 

/*-------------------------------
把DatIn开始的长度位Len位的二进制
复制到DatOut后
--------------------------------*/
void BitsCopy(bool *DatOut, bool *DatIn, int Len)     // 数组复制 OK 
{
	int i = 0;
	for (i = 0; i<Len; i++)
	{
		DatOut[i] = DatIn[i];
	}
}

/*-------------------------------
字节转换成位函数
每8次换一个字节 每次向右移一位
和1与取最后一位 共64位
--------------------------------*/
void ByteToBit(bool *DatOut, char *DatIn, int Num)       // OK
{
	int i = 0;
	for (i = 0; i<Num; i++)
	{
		DatOut[i] = (DatIn[i / 8] >> (i % 8)) & 0x01;
	}
}

/*-------------------------------
位转换成字节函数
字节数组每8次移一位
位每次向左移 与上一次或
---------------------------------*/
void BitToByte(char *DatOut, bool *DatIn, int Num)        // OK
{
	int i = 0;
	for (i = 0; i<(Num / 8); i++)
	{
		DatOut[i] = 0;
	}
	for (i = 0; i<Num; i++)
	{
		DatOut[i / 8] |= DatIn[i] << (i % 8);
	}
}


/*----------------------------------
二进制密文转换为十六进制
需要16个字符表示
-----------------------------------*/
void BitToHex(char *DatOut, bool *DatIn, int Num)
{
	int i = 0;
	for (i = 0; i<Num / 4; i++)
	{
		DatOut[i] = 0;
	}
	for (i = 0; i<Num / 4; i++)
	{
		DatOut[i] = DatIn[i * 4] + (DatIn[i * 4 + 1] << 1)
			+ (DatIn[i * 4 + 2] << 2) + (DatIn[i * 4 + 3] << 3);
		if ((DatOut[i] % 16)>9)
		{
			DatOut[i] = DatOut[i] % 16 + '7';       //  余数大于9时处理 10-15 to A-F
		}                                     //  输出字符 
		else
		{
			DatOut[i] = DatOut[i] % 16 + '0';       //  输出字符	   
		}
	}

}

/*---------------------------------------------
十六进制字符转二进制
----------------------------------------------*/
void HexToBit(bool *DatOut, char *DatIn, int Num)
{
	int i = 0;                        // 字符型输入 
	for (i = 0; i<Num; i++)
	{
		if ((DatIn[i / 4])>'9')         //  大于9 
		{
			DatOut[i] = ((DatIn[i / 4] - '7') >> (i % 4)) & 0x01;
		}
		else
		{
			DatOut[i] = ((DatIn[i / 4] - '0') >> (i % 4)) & 0x01;
		}
	}
}

// 表置换函数  OK
void TablePermute(bool *DatOut, bool *DatIn, const char *Table, int Num)
{
	int i = 0;
	static bool Temp[256] = { 0 };
	for (i = 0; i<Num; i++)                // Num为置换的长度 
	{
		Temp[i] = DatIn[Table[i] - 1];  // 原来的数据按对应的表上的位置排列 
	}
	BitsCopy(DatOut, Temp, Num);       // 把缓存Temp的值输出 
}

// 子密钥的移位
void LoopMove(bool *DatIn, int Len, int Num) // 循环左移 Len数据长度 Num移动位数
{
	static bool Temp[256] = { 0 };    // 缓存   OK
	BitsCopy(Temp, DatIn, Num);       // 将数据最左边的Num位(被移出去的)存入Temp 
	BitsCopy(DatIn, DatIn + Num, Len - Num); // 将数据左边开始的第Num移入原来的空间
	BitsCopy(DatIn + Len - Num, Temp, Num);  // 将缓存中移出去的数据加到最右边 
}

// 按位异或
void Xor(bool *DatA, bool *DatB, int Num)           // 异或函数
{
	int i = 0;
	for (i = 0; i<Num; i++)
	{
		DatA[i] = DatA[i] ^ DatB[i];                  // 异或 
	}
}

// 输入48位 输出32位 与Ri异或
void S_Change(bool DatOut[32], bool DatIn[48])     // S盒变换
{
	int i, X, Y;                                    // i为8个S盒 
	for (i = 0, Y = 0, X = 0; i<8; i++, DatIn += 6, DatOut += 4)   // 每执行一次,输入数据偏移6位 
	{    										  // 每执行一次,输出数据偏移4位
		Y = (DatIn[0] << 1) + DatIn[5];                          // af代表第几行
		X = (DatIn[1] << 3) + (DatIn[2] << 2) + (DatIn[3] << 1) + DatIn[4]; // bcde代表第几列
		ByteToBit(DatOut, &S_Box[i][Y][X], 4);      // 把找到的点数据换为二进制	
	}
}

// F函数
void F_Change(bool DatIn[32], bool DatKi[48])       // F函数
{
	static bool MiR[48] = { 0 };             // 输入32位通过E选位变为48位
	TablePermute(MiR, DatIn, E_Table, 48);
	Xor(MiR, DatKi, 48);                   // 和子密钥异或
	S_Change(DatIn, MiR);                 // S盒变换
	TablePermute(DatIn, DatIn, P_Table, 32);   // P置换后输出
}



void SetKey(char KeyIn[8])               // 设置密钥 获取子密钥Ki 
{
	int i = 0;
	static bool KeyBit[64] = { 0 };                // 密钥二进制存储空间 
	static bool *KiL = &KeyBit[0], *KiR = &KeyBit[28];  // 前28,后28共56
	ByteToBit(KeyBit, KeyIn, 64);                    // 把密钥转为二进制存入KeyBit 
	TablePermute(KeyBit, KeyBit, PC1_Table, 56);      // PC1表置换 56次
	for (i = 0; i<16; i++)
	{
		LoopMove(KiL, 28, Move_Table[i]);       // 前28位左移 
		LoopMove(KiR, 28, Move_Table[i]);	      // 后28位左移 
		TablePermute(SubKey[i], KeyBit, PC2_Table, 48);
		// 二维数组 SubKey[i]为每一行起始地址 
		// 每移一次位进行PC2置换得 Ki 48位 
	}
}

void PlayDes(char MesOut[8], char MesIn[8])  // 执行DES加密
{                                           // 字节输入 Bin运算 Hex输出 
	int i = 0;
	static bool MesBit[64] = { 0 };        // 明文二进制存储空间 64位
	static bool Temp[32] = { 0 };
	static bool *MiL = &MesBit[0], *MiR = &MesBit[32]; // 前32位 后32位 
	ByteToBit(MesBit, MesIn, 64);                 // 把明文换成二进制存入MesBit
	TablePermute(MesBit, MesBit, IP_Table, 64);    // IP置换 
	for (i = 0; i<16; i++)                       // 迭代16次 
	{
		BitsCopy(Temp, MiR, 32);            // 临时存储
		F_Change(MiR, SubKey[i]);           // F函数变换
		Xor(MiR, MiL, 32);                  // 得到Ri 
		BitsCopy(MiL, Temp, 32);            // 得到Li 
	}
	TablePermute(MesBit, MesBit, IPR_Table, 64);
	BitToHex(MesOut, MesBit, 64);
}

void KickDes(char MesOut[8], char MesIn[8])       // 执行DES解密
{												// Hex输入 Bin运算 字节输出 
	int i = 0;
	static bool MesBit[64] = { 0 };        // 密文二进制存储空间 64位
	static bool Temp[32] = { 0 };
	static bool *MiL = &MesBit[0], *MiR = &MesBit[32]; // 前32位 后32位
	HexToBit(MesBit, MesIn, 64);                 // 把密文换成二进制存入MesBit
	TablePermute(MesBit, MesBit, IP_Table, 64);    // IP置换 
	for (i = 15; i >= 0; i--)
	{
		BitsCopy(Temp, MiL, 32);
		F_Change(MiL, SubKey[i]);
		Xor(MiL, MiR, 32);
		BitsCopy(MiR, Temp, 32);
	}
	TablePermute(MesBit, MesBit, IPR_Table, 64);
	BitToByte(MesOut, MesBit, 64);
}

void DESDlg::OnBnClickedButton1()
{
	// TODO: 在此添加控件通知处理程序代码
	/*UpdateData(TRUE);
	char message[64];	//存放明文
	char cipherText[64];	//存放密文
	char message_IP[64];	//存放初始置换（IP）后的数据
	char message_IP1[64];	//逆初始置换（IP-1）
	char message_L[32];	//乘积变换L数组
	char message_R[32];	//乘积变换R数组
	char message_P[8][4];	//P置换
	char Key_change1[48];	//轮密钥生成器生成密钥
	char Key_change2[48];	//轮密钥生成器置换选择2
	char message_E[48];	//扩展变换E
	char key[64];	//存放密钥
	char key_C0[28];	//置换选择1
	char key_D0[28];	//置换选择1

	for(int i=0;i<64;i++)
	{
		message[i]=m_message[i];
		key[i]=m_key[i];
	}

	//初始置换IP矩阵
	int data_IP[8][8]={
		{58,50,42,34,26,18,10,2},
		{60,52,44,36,28,20,12,4},
		{62,54,46,38,30,22,14,6},
		{64,56,48,40,32,24,16,8},
		{57,49,41,33,25,17,9,1},
		{59,51,43,35,27,19,11,3},
		{61,53,45,37,29,21,13,5},
		{63,55,47,39,31,23,15,7}};
	for(int i=0;i<8;i++)
		for(int j=0;j<8;j++)
			message_IP[i*8+j]=message[data_IP[i][j]];

	//16轮变换乘积开始
	//初始化message_L和message_R
	for(int i=0;i<8;i++)
		for(int j=0;j<4;j++)
		{
			message_L[i*4+j]=message_IP[i*6+j];
			message_R[i*4+j]=message_IP[i*6+j+4];
		}
	//轮密钥生成器初始化
	int data_C0[4][7]={
		{57,49,41,33,25,17,9},
		{1,58,50,42,34,26,18},
		{10,2,59,51,43,35,27},
		{19,11,3,60,52,44,36}
	};
	int data_D0[4][7]={
		{63,55,47,39,31,33,15},
		{7,62,54,46,38,30,22},
		{14,6,61,53,45,37,29},
		{21,13,5,28,20,12,4}
	};
	int data_change2[8][6]={
		{14,17,11,24,1,5},{3,28,15,6,21,10},{23,19,12,4,26,9},
		{16,7,27,20,13,2},{41,52,31,37,47,55},{30,30,51,45,33,48},
		{44,49,39,56,34,53},{46,42,50,36,29,32}
	};

	for(int i=0;i<4;i++)
		for(int j=0;j<7;j++)
		{	//置换选择1
			key_C0[i*7+j]=key[data_C0[i][j]];
			key_D0[i*7+j]=key[data_D0[i][j]];
		}


	//定义S1-S8
	int S[8][64]={
	{
		14,4,13,1,2,15,11,8,3,10,6,12,5,9,0,7,
		0,15,7,4,14,2,13,1,10,6,12,11,9,5,3,8,
		4,1,14,8,13,6,2,11,15,12,9,7,3,10,5,0,
		15,12,8,2,4,9,1,7,5,11,3,14,10,0,6,13
	},
	{
		15,1,8,14,6,11,3,4,9,7,2,13,12,0,5,10,
		3,13,4,7,15,2,8,14,12,0,1,10,6,9,11,5,
		0,14,7,11,10,4,13,1,5,8,12,6,9,3,2,15,
		13,8,10,1,3,15,4,2,11,6,7,12,0,5,14,9
	},
	{
		10,0,9,14,6,3,15,5,1,13,12,7,11,4,2,8,
		13,7,0,9,3,4,6,10,2,8,5,14,12,11,15,1,
		13,6,4,9,8,15,3,0,11,1,2,12,5,10,14,7,
		1,10,13,0,6,9,8,7,4,15,14,3,11,5,2,12
	},
	{
		7,13,14,3,0,6,9,10,1,2,8,5,11,12,4,15,
		13,8,11,5,6,15,0,3,4,7,2,12,1,10,14,9,
		10,6,9,0,12,11,7,13,15,1,3,14,5,2,8,4,
		3,15,0,6,10,1,13,8,9,4,5,11,12,7,2,14
	},
	{
		2,12,4,1,7,10,11,6,8,5,3,15,13,0,14,9,
		14,11,2,12,4,7,13,1,5,0,15,10,3,9,8,6,
		4,2,1,11,10,13,7,8,15,9,12,5,6,3,0,14,
		11,8,12,7,1,14,2,13,6,15,0,9,10,4,5,3
	},
	{
		12,1,10,15,9,2,6,8,0,13,3,4,14,7,5,11,
		10,15,4,2,7,12,9,5,6,1,13,14,0,11,3,8,
		9,14,15,5,2,8,12,3,7,0,4,10,1,13,11,6,
		4,3,2,12,9,5,15,10,11,14,1,7,6,0,8,13
	},
	{
		4,11,2,14,15,0,8,13,3,12,9,7,5,10,6,1,
		13,0,11,7,4,9,1,10,14,3,5,12,2,15,8,6,
		1,4,11,13,12,3,7,14,10,15,6,8,0,5,9,2,
		6,11,13,8,1,4,10,7,9,5,0,15,14,2,3,12
	},
	{
		13,2,8,4,6,15,11,1,10,9,3,14,5,0,12,7,
		1,15,13,8,10,3,7,4,12,5,6,11,0,14,9,2,
		7,11,4,1,9,12,14,2,0,6,10,13,15,3,5,8,
		2,1,14,7,4,10,8,13,15,12,9,0,3,5,6,11
	}
	};


	for(int i=0;i<16;i++)
	{
		//轮密钥生成
		int count;	//循环移位数
		if(i==0 || i==1 || i==8)
			count=1;
		else count=2;
		//循环左移
		for(int i=0;i<4;i++)
		{
			char temp[2];
			for(int j=0;j<count;j++)
				temp[j]=key_C0[i*7+j];
			for(int j=0;j<7;j++)
			{	
				key_C0[i*7+j]=key_C0[(i*7+j+count)%8];
			}
			for(int j=0;j<count;j++)
				key_C0[i*7+8-j]=temp[count-1-j];
		}
		char bigBox[56];
		for(int i=0;i<28;i++)
		{
			bigBox[i]=key_C0[i];
			bigBox[i+28]=key_D0[i];
		}
		for(int j=0;j<8;j++)
			for(int k=0;k<6;k++)
				Key_change2[j*6+k]=bigBox[data_change2[i][j]];
		if(i==15)
		{
			//扩展变换E
			int data_E[8][6]={
			{32,1,2,3,4,5},{4,5,6,7,8,9},{8,9,10,11,12,13},{12,13,14,15,16,17},
			{16,17,18,19,20,21},{20,21,22,23,24,25},{24,25,26,27,28,29},{28,29,30,31,32,1}		
			};
			for(int i=0;i<8;i++)
			for(int j=0;j<6;j++)
				message_E[i*6+j]=message_L[data_E[i][j]];
			char message_periorS[48];	//扩展运算E后的48位与轮密钥异或后的结果
			for(int j=0;j<48;j++)
				message_periorS[i]=char((message_E[j]+Key_change2[j])%2+'0');
			//S盒运算
			for(int j=0;j<8;j++)
			{
				int m,n,result;
				m=(message_periorS[j*6+0]-'0')*2+(message_periorS[j*6+5]-'0');
				n=(message_periorS[j*6+1]-'0')*8+(message_periorS[j*6+2]-'0')*4+(message_periorS[j*6+3]-'0')*2+(message_periorS[j*6+4]-'0');
				result=S[j][m*16+n];
				message[j*4+0]=char(result/8+'0');
				message[j*4+1]=char(result/4+'0');
				message[j*4+2]=char(result/2+'0');
				message[j*4+3]=char(result/1+'0');
			}
			//messge_R和message_L异或运算
			for(int j=0;j<32;j++)
				message_R[j]=char(int(message_L[j]+message_R[j])%2+'0');
			break;
		}

		char temp[32];	//中间数组，用于保存message_R，最后赋值给message_L
		for(int j=0;j<32;j++)
			temp[j]=message_R[j];
		//扩展变换E
		int data_E[8][6]={
			{32,1,2,3,4,5},{4,5,6,7,8,9},{8,9,10,11,12,13},{12,13,14,15,16,17},
			{16,17,18,19,20,21},{20,21,22,23,24,25},{24,25,26,27,28,29},{28,29,30,31,32,1}		
		};
		for(int i=0;i<8;i++)
			for(int j=0;j<6;j++)
				message_E[i*6+j]=message_R[data_E[i][j]];
		char message_periorS[48];	//扩展运算E后的48位与轮密钥异或后的结果
		for(int j=0;j<48;j++)
			message_periorS[i]=char((message_E[j]+Key_change2[j])%2+'0');

		//S盒运算
		for(int j=0;j<8;j++)
		{
			int m,n,result;
			m=(message_periorS[j*6+0]-'0')*2+(message_periorS[j*6+5]-'0');
			n=(message_periorS[j*6+1]-'0')*8+(message_periorS[j*6+2]-'0')*4+(message_periorS[j*6+3]-'0')*2+(message_periorS[j*6+4]-'0');
			result=S[j][m*16+n];
			message[j*4+0]=char(result/8+'0');
			message[j*4+1]=char(result/4+'0');
			message[j*4+2]=char(result/2+'0');
			message[j*4+3]=char(result/1+'0');
		}

		//messge_R和message_L异或运算
		for(int j=0;j<32;j++)
			message_R[j]=char(int(message_L[j]+message_R[j])%2+'0');
		for(int j=0;j<32;j++)
			message_L[j]=temp[j];
	}
	for(int i=0;i<8;i++)
		for(int j=0;j<4;j++)
		{
			message_IP1[i*8+j]=message_L[i*4+j];
			message_IP1[i*8+j+4]=message_R[i*4+j];
		}
	int data_IP1[8][8]={
		{40,8,48,16,56,24,64,32},
		{39,7,47,15,55,23,63,31},
		{38,6,46,14,54,22,62,30},
		{37,5,45,13,53,21,61,29},
		{36,4,44,12,52,20,60,28},
		{35,3,43,11,51,19,59,27},
		{34,2,42,10,50,18,58,26},
		{33,1,41,9,49,17,57,25}
	};
	for(int i=0;i<8;i++)
		for(int j=0;j<8;j++)
		{
			cipherText[i*8+j]=message_IP[data_IP1[i][j]];
		}
	m_cipher=cipherText;
	UpdateData(FALSE);

	*/

	UpdateData(true);
		int i = 0;
		char MesHex[17] = { 0 };         // 16个字符数组用于存放 64位16进制的密文
		char *MyKey;          // 初始密钥 8字节*8      
		char *MyMessage;    // 初始明文 

		USES_CONVERSION;
		MyMessage = T2A(m_message);//CString变char指针 
		MyKey = T2A(m_key);
		//memcpy(MyMessage, m_desmiwen, m_desmiwen.GetLength());
		//(m_key.GetLength()));
		//for (int i = 0; i < 8; i++)
			//MyKey[i] = str1[i];
		//memcpy(MyKey, m_desmiyao, m_desmiyao.GetLength());
		//strcpy_s(MyKey, m_desmiyao.GetBuffer(m_desmiyao.GetLength()));
		while (MyKey[i] != '\0')        // 计算密钥长度
		{
			i++;
		}
		while (i != 8)                  // 不是8 提示错误
		{
			//printf("Please input a correct Secret Key!\n");
			//gets(MyKey);
			MessageBox(_T("请输入8位密钥！！"));
			break;
			UpdateData(true);
			MyKey = T2A(m_key);
			//_tcscpy(MyKey, m_desmiyao.GetBuffer(m_desmiyao.GetLength()));
			i = 0;
			while (MyKey[i] != '\0')    // 再次检测
			{
				i++;
			}
		}

		SetKey(MyKey);               // 设置密钥 得到子密钥Ki

		PlayDes(MesHex, MyMessage);   // 执行DES加密
		MesHex[16] = '\0';
		m_cipher = MesHex;
		UpdateData(false);
	 
}

void DESDlg::OnBnClickedButton2()
{
	// TODO: 在此添加控件通知处理程序代码
	CMFC编程Dlg homePage;
	homePage.DoModal();
}
