#pragma once


// RC4Dlg 对话框

class RC4Dlg : public CDialogEx
{
	DECLARE_DYNAMIC(RC4Dlg)

public:
	RC4Dlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~RC4Dlg();

// 对话框数据
	enum { IDD = IDD_RC4_Dlg };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CString message;
	CString key;
	CString cipher;
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton1();
};



#include "MFC编程Dlg.h"
int GetKey(const unsigned char* pass, int pass_len, unsigned char *out);
int RC4(const unsigned char* data, int data_len, const unsigned char* key, int key_len, unsigned char* out, int* out_len); 
static void swap_byte(unsigned char* a, unsigned char* b);

char* Encrypt(const char* szSource, const char* szPassWord); // 加密，返回加密结果
char* Decrypt(const char* szSource, const char* szPassWord); // 解密，返回解密结果

char* ByteToHex(const unsigned char* vByte, const int vLen); // 把字节码pbBuffer转为十六进制字符串，方便传输
unsigned char* HexToByte(const char* szHex); // 把十六进制字符串转为字节码pbBuffer，解码
