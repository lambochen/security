#pragma once


// RSADlg 对话框

class RSADlg : public CDialogEx
{
	DECLARE_DYNAMIC(RSADlg)

public:
	RSADlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~RSADlg();

// 对话框数据
	enum { IDD = IDD_RSA_Dlg };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton2();
	CString message;
	CString cipher;
	afx_msg void OnBnClickedButton1();
};

#include "MFC编程Dlg.h"
