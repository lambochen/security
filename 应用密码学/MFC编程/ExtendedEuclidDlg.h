#pragma once


// ExtendedEuclidDlg 对话框

class ExtendedEuclidDlg : public CDialogEx
{
	DECLARE_DYNAMIC(ExtendedEuclidDlg)

public:
	ExtendedEuclidDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~ExtendedEuclidDlg();

// 对话框数据
	enum { IDD = IDD_ExtendedEuclid_Dlg };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	LONGLONG m_a;
	LONGLONG m_b;
	LONGLONG m_c;
	LONGLONG m_d;
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
};

#include "MFC编程Dlg.h"