#pragma once


// EuclidDlg 对话框

class EuclidDlg : public CDialogEx
{
	DECLARE_DYNAMIC(EuclidDlg)

public:
	EuclidDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~EuclidDlg();

// 对话框数据
	enum { IDD = IDD_Euclid_Dlg };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	LONGLONG m_a;
	LONGLONG m_b;
	LONGLONG m_c;
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
};

#include "MFC编程Dlg.h"
