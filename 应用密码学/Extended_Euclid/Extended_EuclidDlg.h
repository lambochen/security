
// Extended_EuclidDlg.h : 头文件
//

#pragma once


// CExtended_EuclidDlg 对话框
class CExtended_EuclidDlg : public CDialogEx
{
// 构造
public:
	CExtended_EuclidDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_EXTENDED_EUCLID_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	LONGLONG m_a;
	LONGLONG m_b;
	LONGLONG m_c;
	LONGLONG m_d;
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
};
