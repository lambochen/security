
// EuclideanDlg.h : 头文件
//

#pragma once


// CEuclideanDlg 对话框
class CEuclideanDlg : public CDialogEx
{
// 构造
public:
	CEuclideanDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_EUCLIDEAN_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	LONGLONG m_a;
	LONGLONG m_b;
	LONGLONG m_c;
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
};
